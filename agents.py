import numpy as np
import re

class AttackerRules():

    def __init__(self,
        plus_X_to_hit = 0,
        plus_X_to_wound = 0,
        X_mortals_on_Y_to_hit = dict(mrtls=1, on=[]),
        X_mortals_on_Y_to_wound = dict(mrtls=1, on=[]),
        X_additional_mortals_on_Y_to_hit = dict(mrtls=1, on=[]),
        X_additional_mortals_on_Y_to_wound = dict(mrtls=1, on=[]),
        auto_wound_on_X_to_hit = [],
        X_additional_hits_on_Y_to_hit = dict(hits=1, on=[]),
        X_additional_wounds_on_Y_to_wound = dict(wounds=1, on=[]),
        X_extra_dmg_on_Y_to_wound = dict(dmg=1, on=[]),
        X_additional_rend_on_Y_to_hit = dict(rend=1, on=[]),
        reroll_hits = [],
        reroll_wounds = [],
        reroll_damage = []
        ):
        '''
        Parameters
        ----------

            plus_X_to_hit: int
                default: 0

            plus_X_to_wound: int
                default: 0

            X_mortals_on_Y_to_hit: dict
                default: dict(mrtls=1, on=[])
                Attack stop on a hit roll in rolls list, resulting in X mortal wounds instead
                
            X_mortals_on_Y_to_wound: dict
                default: dict(mrtls=1, on=[])
                Attack stop on a wound roll in rolls list, resulting in X mortal wounds instead
                
            X_additional_mortals_on_Y_to_hit: dict
                default: dict(mrtls=1, on=[])
                On a hit roll in rolls list X mortal wounds are dealt after all other damage
                
            X_additional_mortals_on_Y_to_wound: dict
                default: dict(mrtls=1, on=[])
                On a wound roll in rolls list X mortal wounds are dealt after all other damage
                mrtls can be an integer or a list, dictating how many mortals are dealt in each position. 
                For a list, [2, 2, 1] wound indicate that attacks generate 2 mortals for the first 2 attacks that matcch the criteria and then 1 for all subsequent attacks
                
            auto_wound_on_X_to_hit: list
                default: []
                Any rolls to hit in the list result in an automatic wound
                
            X_additional_hits_on_Y_to_hit: dict
                default: dict(hits=1, on=[])
                Any rolls to hit in the list result in X additional hits

            X_additional_wounds_on_Y_to_wound: dict
                default: dict(wounds=1, on=[])
                Any rolls to wound in the list result in X additional wounds
                
            X_extra_dmg_on_Y_to_wound: dict
                default: dict(dmg=1, on=[])
                Any rolls to wound in the list result in X extra damage
                
            X_additional_rend_on_Y_to_hit: dict
                default: dict(rend=1, on=[])
                Any rolls to wound in the list result in X additonal AP 
                
            reroll_hits: list, str
                default: []
                                
            reroll_wounds: list, str
                default: []

            reroll_damage: list
                default: []
                Note that this applies to the raw roll, before any added damage. 
                It does however apply after things that affect the way the roll happens (e.g. old melta, i.e. 2D6 pick the highest)
                
        '''
        #Sanitise some inputs
        assert ((type(reroll_hits) == list) | (reroll_hits in ['fails']))
        assert ((type(reroll_wounds) == list) | (reroll_wounds in ['fails']))

        self.plus_X_to_hit = plus_X_to_hit
        self.plus_X_to_wound = plus_X_to_wound
        self.X_mortals_on_Y_to_hit = X_mortals_on_Y_to_hit
        self.X_mortals_on_Y_to_wound = X_mortals_on_Y_to_wound
        self.X_additional_mortals_on_Y_to_hit = X_additional_mortals_on_Y_to_hit
        self.X_additional_mortals_on_Y_to_wound = X_additional_mortals_on_Y_to_wound
        self.auto_wound_on_X_to_hit = auto_wound_on_X_to_hit
        self.X_additional_hits_on_Y_to_hit = X_additional_hits_on_Y_to_hit
        self.X_additional_wounds_on_Y_to_wound = X_additional_wounds_on_Y_to_wound
        self.X_extra_dmg_on_Y_to_wound = X_extra_dmg_on_Y_to_wound
        self.X_additional_rend_on_Y_to_hit = X_additional_rend_on_Y_to_hit
        self.reroll_hits = reroll_hits
        self.reroll_wounds = reroll_wounds
        self.reroll_damage = reroll_damage

class DefenderRules():

    def __init__(self,
        minus_X_to_hit = 0,
        minus_X_to_wound = 0,
        minus_X_damage = 0,
        feel_no_pain = 7,
        feel_no_pain_mortals=7):

        '''
        minus_X_to_hit: int
            default: 0

        minus_X_to_wound: int
            default: 0

        minus_X_to_damage: int
            default: 0

        feel_no_pain: int
            default: 7

        feel_no_pain_mortals: int
            default: 7
        '''

        self.minus_X_to_hit = minus_X_to_hit
        self.minus_X_to_wound = minus_X_to_wound
        self.minus_X_damage = minus_X_damage
        self.feel_no_pain = feel_no_pain
        self.feel_no_pain_mortals = feel_no_pain_mortals


class Weapon():

    def __init__(
        self,
        n_attacks,
        skill,
        strength,
        armour_penetration,
        damage,
        rules = AttackerRules()
        ):
        '''
        Parameters
        ----------

            n_attacks : int, str
                Either an int or "<X>D<Y>[M<Z>]" where X is the number of dice and Y are the type of dice. 
                Append "M<Z>" to set the minimum (e.g. blast)

            skill : int
                The WS/BS

            strength : int
                The attack strength

            armour_penetration : int
                The attack AP

            damage : int, str
                Either and int flat value or "<X>D<Y>[M]" where X is the number of dice and Y are the type of dice.
                Append "M" for old melta rules (roll 2 dice and pick the highest)

            rules : AttackerRules
                An AdditionalRules object with any special rules
        '''

        self.n_attacks = n_attacks
        self.skill = skill
        self.strength = strength
        self.armour_penetration = armour_penetration
        self.damage = damage

        self.rules = rules


    def calculate_rerolls(self, hit_on, wound_on):
        '''
        Convert reroll strings to actual values
        '''

        #Setup rules
        if (self.rules.reroll_hits == "fails"):
            self.rules.reroll_hits = list(range(1, hit_on))
        if (self.rules.reroll_wounds == "fails"):
            self.rules.reroll_wounds = list(range(1, wound_on))




class Attacker():

    def __init__(self, 
        name,
        weapons = [],
        points = 0
        ):
        '''
        Parameters
        ----------

            name : str
                The name of the attacker

            weapons : list
                The list of weapons objects, in the order in which they are to be used
        '''
        self.name = name
        self.weapons = weapons
        self.points = points


    def add_weapon(self, weapon):
        self.weapons.append(weapon)

class Defender():

    def __init__(
        self,
        name,
        toughness,
        wounds,
        save,
        invuln,
        mode,
        rules=DefenderRules(),
        points=0
        ):
        '''
        Parameters
        ----------

            name : str

            toughness : int

            wounds : int

            save : int

            invuln : int

            mode : str
                In 'single' or 'unit' 

            rules:
                DefenderRules object

        '''

        self.name = name
        self.toughness = toughness
        self.wounds = wounds
        self.save = save
        self.invuln = invuln
        self.mode = mode
        self.rules = rules
        self.points = points

        #For tracking damage and casualties
        self.reset()

    def get_info_dict(self):
        return {
            'casualties' : self.casualties,
            'current_damage_taken' : self.current_damage_taken
            }

    def apply_damages(self, damages):
        '''
        damages : list of ints
        '''

        #----------------------------------------------------------------------
        # APPLY DAMAGE
        #----------------------------------------------------------------------

        if self.mode == 'single':

            self.take_damage(sum(damages))

        elif self.mode == 'unit':
            #Loop to apply damage and FNP
            for damage in damages:
                self.take_damage(damage)


    def take_damage(self, damage):
        '''
        damage : int
        '''
        #----------------------------------------------------------------------
        # APPLY FEEL NO PAIN
        #----------------------------------------------------------------------
        fnp_roll = np.random.randint(1, 7, damage)
        self.current_damage_taken += sum(fnp_roll < self.rules.feel_no_pain)

        if self.current_damage_taken >= self.wounds:

            if self.mode == 'single':
                self.casualties = 1

            elif self.mode == 'unit':
                self.casualties += 1
                self.current_damage_taken = 0

    def take_mortals(self, mortals):
        '''
        mortals : int
        '''
        #----------------------------------------------------------------------
        # APPLY FEEL NO PAIN
        #----------------------------------------------------------------------
        for _ in range(mortals):
            fnp_roll = np.random.randint(1, 7)
            self.current_damage_taken += sum(fnp_roll < self.rules.feel_no_pain_mortals)

            if self.current_damage_taken >= self.wounds:

                if self.mode == 'single':
                    self.casualties = 1

                elif self.mode == 'unit':
                    self.casualties += 1
                    self.current_damage_taken = 0

    def reset(self):
        self.casualties = 0
        self.current_damage_taken = 0 

