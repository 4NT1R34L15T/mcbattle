import ZODB, ZODB.FileStorage

from agents import Attacker, Defender, AttackerRules, Weapon, DefenderRules



'''
#method 1
conn = db.open()
conn.root.foo = 1
db.close()

#method 2
with db.transaction() as connection:
    connection.root.foo = 1
'''

#%%
#Tests

class DBStart():

    def __init__(self, name):
        self.dbname = name


    def __enter__(self):
        storage = ZODB.FileStorage.FileStorage(self.dbname)
        self.db = ZODB.DB(storage)

        return self.db


    def __exit__(self, exc_type, exc_value, exc_traceback):

        self.db.close()


class DBConnect():

    def __init__(self, name='./dbs/mydata.fs'):

        self.dbname = name

        with DBStart(self.dbname) as db:

            with db.transaction() as conn:
            
                if not 'attackers' in conn.root().keys():
                    conn.root.attackers = {}

                if not 'defenders' in conn.root():
                    conn.root.defenders = {}


    def save_object(self, obj):

        with DBStart(self.dbname) as db:

            if type(obj) == Attacker:
                #Create connection
                with db.transaction() as conn:
                    print(obj.name)
                    conn.root.attackers[obj.name] = obj
                    print(conn.root())
                    print(conn.root)

            elif type(obj) == Defender:
                #Create connection
                with db.transaction() as conn:
                    conn.root.defenders[obj.name] = obj

    def get_object_names(self):
        with DBStart(self.dbname) as db:
            with db.transaction() as conn:

                objs = dict(
                    attackers = list(conn.root.attackers.keys()),
                    defenders = list(conn.root.defenders.keys())
                    )

                return objs

    def get_attacker(self, obj_name):
        with DBStart(self.dbname) as db:
            with db.transaction() as conn:
                if not obj_name in conn.root.attackers.keys():
                    print(f"{obj_name} not found in attackers")
                    return None
                else:
                    return(conn.root.attackers[obj_name])


    def get_defender(self, obj_name):
        with DBStart(self.dbname) as db:
            with db.transaction() as conn:
                if not obj_name in conn.root.defenders.keys():
                    print(f"{obj_name} not found in defenders")
                    return None
                else:
                    return(conn.root.defenders[obj_name])

# %%