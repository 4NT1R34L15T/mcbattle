#%%
import seaborn as sns
import pandas as pd

from agents import Attacker, Defender, AttackerRules, Weapon, DefenderRules
from mcsim import Simulation
from dbmanage import DBConnect

a1 = Attacker(
    name  = '20 Wyches (WS2, RR W)',
    points = 260,
    weapons =  [
        Weapon(
            n_attacks = 8,
            skill = 2,
            strength = 4,
            armour_penetration = 2,
            damage = 2,
            rules = AttackerRules(
                X_additional_rend_on_Y_to_hit={'rend':1, 'on':[6]},
                reroll_wounds='fails'
                )
            ),
        Weapon(
            n_attacks = 73,
            skill = 2,
            strength = 4,
            armour_penetration = 1,
            damage = 1,
            rules = AttackerRules(
                X_additional_rend_on_Y_to_hit={'rend':1, 'on':[6]},
                reroll_wounds='fails'
                )
            )
    ])

a2 = Attacker(
    name  = '20 Wyches (WS2, RR W, S5)',
    points = 260,
    weapons =  [
        Weapon(
            n_attacks = 8,
            skill = 2,
            strength = 5,
            armour_penetration = 2,
            damage = 2,
            rules = AttackerRules(
                X_additional_rend_on_Y_to_hit={'rend':1, 'on':[6]},
                reroll_wounds='fails'
                )
            ),
        Weapon(
            n_attacks = 73,
            skill = 2,
            strength = 5,
            armour_penetration = 1,
            damage = 1,
            rules = AttackerRules(
                X_additional_rend_on_Y_to_hit={'rend':1, 'on':[6]},
                reroll_wounds='fails'
                )
            )
    ])


a3 = Attacker(
    name='Redemptor Capt Leuit',
    points=185,
    weapons= [
        Weapon(
            n_attacks = "D6",
            skill = 3,
            strength = 9,
            armour_penetration = 4,
            damage = 3,
            rules = AttackerRules(
                reroll_hits=[1],
                reroll_wounds=[1]
                )
            ),
        Weapon(
            n_attacks = 8,
            skill = 3,
            strength = 5,
            armour_penetration = 1,
            damage = 1,
            rules = AttackerRules(
                reroll_hits=[1],
                reroll_wounds=[1]
                )
            ),
        Weapon(
            n_attacks = "D3",
            skill = 3,
            strength = 7,
            armour_penetration = 1,
            damage = 2,
            rules = AttackerRules(
                reroll_hits=[1],
                reroll_wounds=[1]
                )
            ),
        Weapon(
            n_attacks = 8,
            skill = 3,
            strength = 4,
            armour_penetration = 0,
            damage = 1,
            rules = AttackerRules(
                reroll_hits=[1],
                reroll_wounds=[1]
                )
            )
    ])

d1 = Defender(
    name ="Dreadknight",
    toughness = 6,
    wounds = 13,
    save = 2,
    invuln = 4,
    mode = 'single',
    rules = DefenderRules(
        feel_no_pain_mortals= 5
    ))

d2 = Defender(
    name ="Talos",
    toughness = 7,
    wounds = 7,
    save = 3,
    invuln = 6,
    mode = 'unit',
    points = 100,
    rules = DefenderRules(
        minus_X_damage=1, 
        feel_no_pain= 5
    ))

d3 = Defender(
    name ="Grot",
    toughness = 6,
    wounds = 4,
    save = 6,
    invuln = 6,
    mode = 'unit',
    points=35,
    rules = DefenderRules(
        minus_X_damage=1, 
        feel_no_pain= 5
    ))

d4 = Defender(
    name ="Grot (4+))",
    toughness = 6,
    wounds = 4,
    save = 4,
    invuln = 6,
    mode = 'unit',
    points=35,
    rules = DefenderRules(
        minus_X_damage=1, 
        feel_no_pain= 5
    ))

d5 = Defender(
    name ="Cronos",
    toughness = 7,
    wounds = 7,
    save = 3,
    invuln = 6,
    mode = 'unit',
    rules = DefenderRules(
        minus_X_damage=1, 
        feel_no_pain= 5
    ),
    points = 75)

d6 = Defender(
    name ="Knight Magaera",
    toughness = 8,
    wounds = 24,
    save = 3,
    invuln = 5,
    mode = 'single',
    points = 0)

#%% Save objects to database
attackers = [a1, a2, a3]
defenders = [d1, d2, d3, d4, d5, d6]

dbc = DBConnect()

for o in attackers + defenders:
    dbc.save_object(o)

dbc.get_object_names()


#%% 
s = Simulation(
        100,
        [a1, a2, a3],
        [d2, d3, d4, d5, d6])
s.run()
s.display()

ca, df_pivot, df_styles = s.cost_analysis()

#%%
