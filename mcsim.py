
#Imports
from os import altsep
from typing import get_type_hints
import numpy as np
from numpy.core.fromnumeric import mean
import pandas as pd
from pandas.tseries import frequencies
from tqdm import tqdm
import re
import matplotlib.pyplot as plt

from agents import Attacker, Defender, AttackerRules, Weapon
from rolls import Rolls
import seaborn as sns

class Simulation():
    '''
    Simulation manages the actual iteration over the attacker and defender
    '''

    def __init__(
        self,
        n_mc_iters,
        attackers,
        defenders):
        '''
        Parameters
        ----------

            attackers : list
                List of Attacker objects 

            defender : list
                A list of defender objects
        '''

        self.attackers = attackers
        self.defenders = defenders
        self.n_mc_iters = n_mc_iters

        self.reset()

    def reset(self):
        self.results = {}
        self.rolls = {}

    def run(self):
        ##Iterate over defenders:
        for defender in self.defenders:
            self.results[defender.name] = {}
            self.rolls[defender.name] = {}
            #Iterate over attackers
            for attacker in self.attackers:
                self.results[defender.name][attacker.name] = []
                self.rolls[defender.name][attacker.name] = []
            
                #Iterate n_mc_iters times
                for iter in tqdm(range(self.n_mc_iters)):
                    defender.reset()

                    #Iterate through weapons
                    for weapon in attacker.weapons:

                        rolls = Rolls(weapon, defender)

                        #Do rolls
                        rolls.roll_hits()
                        rolls.roll_wounds()
                        rolls.roll_saves(defender)
                        #Apply damage
                        defender.apply_damages(rolls.damages)
                        defender.take_mortals(rolls.mortals)

                        self.rolls[defender.name][attacker.name].append(rolls)

                    self.results[defender.name][attacker.name].append(defender.get_info_dict())

    def cost_analysis(self, prescision=1):
        '''
        Analyses the damage done and taken per point

        '''
        ca_results = []
        for defender in self.defenders:

            for attacker in self.attackers:

                #Place in dataframe for analysis
                results_df = pd.DataFrame(self.results[defender.name][attacker.name])

                if defender.mode == "single":
                    mean_kills = results_df['current_damage_taken'].mean() / defender.wounds
                    mean_damage = results_df['current_damage_taken'].mean()

                elif defender.mode == 'unit':
                    mean_kills = results_df['casualties'].mean() + (results_df['current_damage_taken'] / defender.wounds).mean()
                    mean_damage = results_df['casualties'].mean() * defender.wounds + results_df['current_damage_taken'].mean()

                #Attacker point efficiency
                da_dict = {
                    'attacker_name' : attacker.name,
                    'defender_name' : defender.name,
                    'mean_damage' :  mean_damage,
                    'mean_kills' : mean_kills}

                da_dict['damage_per_point'] = None if (attacker.points == 0) else mean_damage / attacker.points
                da_dict['points_destroyed'] = None if (defender.points == 0) else mean_kills * defender.points
                da_dict['points_destroyed_per_point'] = None if ((attacker.points == 0) | (defender.points == 0)) else mean_kills * defender.points / attacker.points

                ca_results.append(da_dict)

        #Convert to dict of pivots
        #Cost analysis
        ca_df = pd.DataFrame(ca_results).round(prescision)
        ca_df_p_dict = {}
        ca_df_p_styles_dict = {}
        for factor in [x for x in ca_df.columns if x not in ['attacker_name', 'defender_name']]:
            ca_df_p = ca_df.pivot(
                index='attacker_name', 
                columns='defender_name', 
                values=factor
            )
            #Save raw data
            ca_df_p_dict[factor] = ca_df_p

            #Save style
            cm = sns.color_palette("vlag", as_cmap=True)
            pd.set_option('colheader_justify', 'center')
            st = ca_df_p.style.background_gradient(cmap=cm, axis=1)
            ca_df_p_styles_dict[factor] = st
                
        return ca_results, ca_df_p_dict, ca_df_p_styles_dict



    def display(self):

        figs = {}

        #Iterate through defenders
        for defender in self.defenders:

            colors = plt.get_cmap("tab10")
            max_dmg = 0

            #Set up plot
            fig = plt.figure(figsize=(15, 10), facecolor='lightgray')
            plt.title(f"vs {defender.name}")

            #Loop through attackers
            for idx, attacker in enumerate(self.results[defender.name].keys()):

                #Place in dataframe for analysis
                results_df = pd.DataFrame(self.results[defender.name][attacker])

                #If single
                if defender.mode == "single":
                    #Summary stats
                    p_destroy = results_df['casualties'].mean()
                    mean_damage = results_df['current_damage_taken'].mean()
                    #Get PMF and CMF
                    outcomes, frequencies = np.unique(results_df['current_damage_taken'], return_counts=True)
                    pmf = frequencies / frequencies.sum()
                    cdf = np.array(np.cumsum(pmf[::-1])[::-1])

                    #Mean damage
                    label = attacker + "\nMean: " + str(round(mean_damage, 2))

                    #Plot CMF
                    plt.plot(outcomes, cdf, label=label, color=colors(idx))

                    #Plot points of interest
                    plt.scatter([defender.wounds], [p_destroy], color=colors(idx))
                    plt.annotate(f"{round(p_destroy * 100, 1)}%", [defender.wounds,p_destroy], 
                        color=colors(idx), textcoords="offset points", xytext=(5, 0))

                elif defender.mode == "unit":
                    #Summary stats
                    mean_kills = results_df['casualties'].mean() + (results_df['current_damage_taken'] / defender.wounds).mean()
                    #Get PMF and CMF
                    outcomes, frequencies = np.unique(results_df['casualties'], return_counts=True)
                    pmf = frequencies / frequencies.sum()
                    cdf = np.array(np.cumsum(pmf[::-1])[::-1])

                    #Print mean kills
                    label = attacker + "\nMean: " + str(round(mean_kills, 2))

                    #Plot CMF
                    plt.bar(outcomes, cdf, label=label, color=colors(idx), 
                        width=0.1, alpha=0.2)
                    #Annotate
                    for annotation_idx, outcome in enumerate(outcomes):
                        plt.annotate(f"{round(cdf[annotation_idx]*100, 2)}%", [outcome, cdf[annotation_idx]], 
                        color=colors(idx), textcoords="offset points", xytext=(15, 0))

                #Update max damage
                max_dmg = np.max([max_dmg, outcomes[-1]])

            #Plot reference lines
            if defender.mode == 'single':
                plt.xlabel('of doing at least X damage')
                plt.plot([defender.wounds, defender.wounds], [0, 1], 'b--', alpha=0.3, label="Target Wounds")
                plt.plot([-0.5, max_dmg], [0.5, 0.5], 'r--', alpha=0.3, label="50% likelihood threshold")

            plt.legend()

            figs[defender.name] = fig

                

        return figs





                    
        

    




