import numpy as np
import re


class Rolls():

    def __init__(self, weapon, defender):

        self.hit_rolls = {
            "initial" : np.array([]),
            "rerolled" : np.array([]),
            "final" : np.array([]),
            "successful" : np.array([])}
        self.wound_rolls = {
            "initial" : np.array([]),
            "rerolled" : np.array([]),
            "final" : np.array([]),
            "successful" : np.array([]),
            "save_failed" : np.array([])}
        self.save_rolls = np.array([])
        self.armour_penetration = np.array([])
        self.damages = np.array([])
        self.mortals = 0

        self.weapon = weapon

        hit_on, wound_on = self.get_hits_wounds_on(weapon, defender)

        #Process weapon
        self.weapon.calculate_rerolls(hit_on, wound_on)

        self.hit_on = hit_on
        self.wound_on = wound_on

        #For tracking single reroll (e.g expert crafters, CP)
        self.available_rerolls_hit = 0
        self.available_rerolls_wound = 0

    @staticmethod
    def get_hits_wounds_on(weapon, defender):
        #Calculate what rolls the weapon hits on
        hit_on = np.clip(weapon.skill + np.clip(weapon.rules.plus_X_to_hit + defender.rules.minus_X_to_hit, -1, 1), 2, 6)
        #Calculate what rolls the weapon wounds on
        if (weapon.strength >= 2*defender.toughness):
            wound_on_pre_mod = 2
        elif (weapon.strength > 2*defender.toughness):
            wound_on_pre_mod = 3
        elif (weapon.strength == defender.toughness):
            wound_on_pre_mod = 4
        elif (weapon.strength*2 <= defender.toughness):
            wound_on_pre_mod = 6
        else:
            wound_on_pre_mod = 5

        wound_on = np.clip(wound_on_pre_mod + np.clip(weapon.rules.plus_X_to_wound + defender.rules.minus_X_to_wound, -1, 1), 2, 6)

        return hit_on, wound_on

    

    @staticmethod
    def get_matches(roll, rolls_to_match):
        '''
        Splits a roll into two rolls
        The first returned list contains all the rolls that are in rolls_to_match
        The second contains all the rolls that are not in rolls_to_match

        E.g.
        get_matches([1,2,3,1,2,3], [1,2])
        > [1,2,1,2], [3, 3]
        '''

        if rolls_to_match == []:
            non_matches = roll
            matches = []

        else:
            non_matches = [x for x in roll if x not in rolls_to_match]
            matches = [x for x in roll if x in rolls_to_match]
        
        return np.array(matches), np.array(non_matches)

    def roll_hits(self):
        '''
        Sets the hit_rolls parameter of this class to the sequence of dice rolled.
        Returns the number of successful hits.

        Parameters
        ----------
            weapon : Weapon

        '''

        #----------------------------------------------------------------------
        # ROLL HITS
        #----------------------------------------------------------------------
        
        #Normal
        if type(self.weapon.n_attacks) == int:
            n_attacks = self.weapon.n_attacks
        #Random shots
        elif type(self.weapon.n_attacks) == str:
            hit_number_dice = 1 if self.weapon.n_attacks[0] == "D" else int(re.findall(r"^(\d*?)D", self.weapon.n_attacks)[0])
            hit_dice_type = int(re.findall(r"\D+?(\d+)", self.weapon.n_attacks)[0])

            n_attacks_roll = np.random.randint(1, hit_dice_type+1, hit_number_dice)

            #Blast etc.
            if "M" in self.weapon.n_attacks>1:
                min_attacks = int(re.findall(r"M(\d+)", self.weapon.n_attacks)[0])
                n_attacks_roll[n_attacks_roll < min_attacks]=min_attacks

            n_attacks = sum(n_attacks_roll)

        #Actually roll the hits
        self.hit_rolls["initial"] = np.random.randint(1, 7, n_attacks)

        #----------------------------------------------------------------------
        # REROLL HITS
        #----------------------------------------------------------------------
        
        #Normal rerolls
        hits_to_reroll, self.hit_rolls["initial"] = self.get_matches(
            self.hit_rolls["initial"], 
            self.weapon.rules.reroll_hits)
        self.hit_rolls["rerolled"] = np.random.randint(1, 7, len(hits_to_reroll))
        
        #Expert crafters style rerolls
        n_individual_rerolls = min(self.available_rerolls_hit, sum(self.hit_rolls["initial"] < self.hit_on))
        if (n_individual_rerolls > 0 ):
            self.hit_rolls['rerolled'] = np.concatenate([
                self.hit_rolls["rerolled"],
                np.random.randint(1, 7, n_individual_rerolls)])
            self.hit_rolls["initial"] = sorted(self.hit_rolls["initial"])[n_individual_rerolls : ]
            #Decrement available rerolls
            self.available_rerolls_hit -= n_individual_rerolls

        self.hit_rolls["final"] = np.concatenate([
            self.hit_rolls["initial"],
            self.hit_rolls["rerolled"]])

        #Record successful hits
        self.hit_rolls['successful'] = np.array(self.hit_rolls['final'])[
            self.hit_rolls['final'] >= self.hit_on
        ]

    def get_n_hits(self):
        #----------------------------------------------------------------------
        # COUNT HITS
        #----------------------------------------------------------------------

        '''
        TODO integrate mortal wounds
        '''

        #Count hits
        n_hits = len(self.hit_rolls['successful'])

        #Add exploding hits from hit rolls
        n_additional_hits = int(self.weapon.rules.X_additional_hits_on_Y_to_hit['hits'])
        additional_hits_on = self.weapon.rules.X_additional_hits_on_Y_to_hit['on']
        n_hits += sum([n_additional_hits for x in self.hit_rolls["final"] if x in additional_hits_on])

        return n_hits

    def roll_wounds(self):

        n_hits = self.get_n_hits()

        #Check for any automatic wounds
        wounded_prior, _  = self.get_matches(
            self.hit_rolls['final'], 
            self.weapon.rules.auto_wound_on_X_to_hit)

        n_wounds = len(wounded_prior)
        n_wound_rolls = n_hits - n_wounds

        #----------------------------------------------------------------------
        # ROLL WOUNDS
        #----------------------------------------------------------------------

        #Actually roll the wounds
        self.wound_rolls['initial'] = np.random.randint(1, 7, n_wound_rolls)

        #----------------------------------------------------------------------
        # REROLL WOUNDS
        #----------------------------------------------------------------------

        #Normal rerolls
        wounds_to_reroll, self.wound_rolls["initial"] = self.get_matches(
            self.wound_rolls["initial"], 
            self.weapon.rules.reroll_wounds)

        self.wound_rolls['rerolled'] = np.random.randint(1, 7, len(wounds_to_reroll))

        #Expert crafters style rerolls
        n_individual_rerolls = min(self.available_rerolls_wound, sum(self.wound_rolls["initial"] < self.wound_on))
        if (n_individual_rerolls > 0 ):
            self.wound_rolls['rerolled'] = np.concatenate([
                self.wound_rolls["rerolled"],
                np.random.randint(1, 7, n_individual_rerolls)])
            self.wound_rolls["initial"] = sorted(self.wound_rolls["initial"])[n_individual_rerolls : ]
            #Decrement available rerolls
            self.available_rerolls_wound -= n_individual_rerolls

        self.wound_rolls["final"] = np.concatenate([
            self.wound_rolls["initial"],
            self.wound_rolls["rerolled"]])

        self.wound_rolls['successful'] = np.array(self.wound_rolls["final"])[
            np.array(self.wound_rolls["final"]) >= self.wound_on]

        #----------------------------------------------------------------------
        # MORTALS (FROM WOUNDS)
        #----------------------------------------------------------------------
        if self.weapon.rules.X_additional_mortals_on_Y_to_wound['on'] != []:

            matches, _ = self.get_matches(self.wound_rolls['successful'], 
                self.weapon.rules.X_additional_mortals_on_Y_to_wound['on'])
            
            no_mortals = self.weapon.rules.X_additional_mortals_on_Y_to_wound['mrtls']
            if type(no_mortals) == int:
                self.mortals += len(matches) * no_mortals
            elif type(no_mortals) == list:
                self.mortals += sum(no_mortals[:len(matches)]) + no_mortals[-1] * min([len(matches) - len(no_mortals), 0])


        

    def get_n_wounds(self):
        #----------------------------------------------------------------------
        # COUNT WOUNDS
        #----------------------------------------------------------------------

        n_wounds  = len(self.wound_rolls['successful'])
        additional_wounds = 0

        #X_additional_wounds_on_Y_to_wound
        if self.weapon.rules.X_additional_wounds_on_Y_to_wound['on'] != []:
            matches, _ = self.get_matches(self.wound_rolls['successful'], 
                self.weapon.rules.X_additional_wounds_on_Y_to_wound['on'])

            additional_wounds = len(matches) * self.weapon.rules.X_additional_wounds_on_Y_to_wound['wounds']

        return(n_wounds, additional_wounds)

    def calculate_armour_penetration(self, n_additional_wounds=0):
        #Work out AP
        if self.weapon.rules.X_additional_rend_on_Y_to_hit['on'] == []:
            self.armour_penetration = np.array([self.weapon.armour_penetration]*len(self.wound_rolls['successful']))

        else:
            n_additional_rend = self.weapon.rules.X_additional_rend_on_Y_to_hit['rend']
            additional_rend_on = self.weapon.rules.X_additional_rend_on_Y_to_hit['on']
            armour_penetration_mod = np.array(
                [n_additional_rend if x in additional_rend_on else 0 for x in self.wound_rolls['successful']]
            )
            self.armour_penetration = armour_penetration_mod + self.weapon.armour_penetration

        self.armour_penetration = np.concatenate([
            self.armour_penetration,
            [self.weapon.armour_penetration] * n_additional_wounds])

    def roll_saves(self, defender):

        n_wounds, n_additional_wounds = self.get_n_wounds()

        self.calculate_armour_penetration(n_additional_wounds)

        #----------------------------------------------------------------------
        # ROLL SAVES
        #----------------------------------------------------------------------

        self.save_rolls = np.random.randint(1, 7, n_wounds + n_additional_wounds)

        #----------------------------------------------------------------------
        # CHECK SAVES
        #----------------------------------------------------------------------

        failed_saves = self.save_rolls < np.min(
            [self.armour_penetration + defender.save,
            [defender.invuln]*len(self.armour_penetration)], axis=0)

        #Save only failed wounds
        self.wound_rolls['save_failed'] = np.concatenate([
            self.wound_rolls['successful'],
            [0] * n_additional_wounds
            ])[failed_saves]

        self.damages = self.calculate_damage(defender)

    def calculate_damage(self, defender):

        #----------------------------------------------------------------------
        # WORK OUT DAMAGE
        #----------------------------------------------------------------------

        #Base damage
        damage_add = 0
        if type(self.weapon.damage) == int:
            damages = [self.weapon.damage] * len(self.wound_rolls['save_failed'])
        elif type(self.weapon.damage) == str:
            damage_number_dice = 1 if self.weapon.damage[0] == "D" else int(re.findall(r"^(\d+)D", self.weapon.damage)[0])
            damage_dice_type = int(re.findall(r"D(\d+)", self.weapon.damage)[0])
            #Find damage modifiers
            damage_mod_find = re.findall(r"([\+])(\d+)", self.weapon.damage)
            if damage_mod_find != []:
                damage_add = int(damage_mod_find[0][1])
            #Melta
            damage_melta = "M" in self.weapon.damage

            damages = np.random.randint(1, damage_dice_type, damage_number_dice) + \
                damage_melta * np.random.randint(1, damage_dice_type, damage_number_dice)

            #Reroll damage
            if self.weapon.rules.reroll_damages != []:
                damages_to_reroll, damages_to_keep = self.get_matches(damages, self.weapon.rules.reroll_damages)
                damages_rerolled = np.random.randint(1, damage_dice_type, len(damages_to_reroll)) + \
                    damage_melta * np.random.randint(1, damage_dice_type, len(damages_to_reroll))

                #consolidate damages
                damages = np.concatenate([
                    damages_to_keep, damages_rerolled
                ])
                    
            #Add additional damages
            damages = damages + damage_add


        #Extra damage based on wound rolls
        if self.weapon.rules.X_extra_dmg_on_Y_to_wound['on'] != []:
            extra_damage = self.weapon.rules.X_extra_dmg_on_Y_to_wound['dmg']
            for each_roll in self.weapon.rules.X_extra_dmg_on_Y_to_wound['on']:
                damages = np.add(
                    damages,
                    (np.array(self.wound_rolls['final']) == each_roll) * extra_damage)

        #Make sure np.array
        damages = np.array(damages)

        #Subtract damage for defender
        damages -= defender.rules.minus_X_damage

        #Minimum 1 damamge
        damages = np.maximum(damages, 1)

        return damages